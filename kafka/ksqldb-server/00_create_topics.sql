
CREATE SOURCE CONNECTOR calls_reader WITH (
    'connector.class' = 'io.debezium.connector.mysql.MySqlConnector',
    'database.hostname' = 'mysql',
    'database.port' = '3306',
    'database.user' = 'example-user',
    'database.password' = 'example-pw',
    'database.allowPublicKeyRetrieval' = 'true',
    'database.server.id' = '223344',
    'topic.prefix' = 'call-center-data-changes',
    'schema.history.internal.kafka.bootstrap.servers' = 'broker:29092',
    'schema.history.internal.kafka.topic' = 'call-center-schema-changes',
    'database.whitelist' = 'call-center',
    'table.whitelist' = 'call-center.calls',
    'include.schema.changes' = 'false'
);
