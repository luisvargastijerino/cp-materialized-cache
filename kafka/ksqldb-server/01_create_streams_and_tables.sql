
SHOW TOPICS;

ASSERT TOPIC 'call-center-data-changes.call-center.calls' TIMEOUT 50 SECONDS;

SET 'auto.offset.reset' = 'earliest';

CREATE STREAM calls WITH (
    kafka_topic = 'call-center-data-changes.call-center.calls',
    value_format = 'avro'
);

CREATE TABLE support_view AS
    SELECT after->name AS name,
           count_distinct(after->reason) AS distinct_reasons,
           latest_by_offset(after->reason) AS last_reason
    FROM calls
    GROUP BY after->name
    EMIT CHANGES;

CREATE TABLE lifetime_view AS
    SELECT after->name AS name,
           count(after->reason) AS total_calls,
           (sum(after->duration_seconds) / 60) as minutes_engaged
    FROM calls
    GROUP BY after->name
    EMIT CHANGES;
