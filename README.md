# Materialized cache

Based on https://docs.ksqldb.io/en/latest/tutorials/materialized/

## Run

Next command will start mysql database and kafka components. 

```shell
docker compose up -d
```

## Stop and Delete

```shell
docker compose down -v --remove-orphans
```